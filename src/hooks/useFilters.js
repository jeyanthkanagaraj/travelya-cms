import { useRouter } from 'next/router'
import qs from 'qs'
import { DEFAULT_FILTERS } from 'lib/constants'
import { cleanObject } from 'lib/objectUtils'

export const makeDisplayUrls = (filters, baseUrl, as) => {
    const defaultFilters = {
        ...DEFAULT_FILTERS,
    }
    const f = qs.stringify({ ...defaultFilters, ...filters })
    const href = `/${baseUrl}?${f}`
    const newAs = `${as}?${f}` || href

    return {
        newAs,
        href
    }
}

export const useFilters = (filters, baseUrl, as) => {
    const router = useRouter()
    const onApply = formState => {
        const filterState = cleanObject({
            ...filters,
            page: 1,
            ...formState
        })

        const { newAs, href } = makeDisplayUrls(filterState, baseUrl, as)
        router.push(href, newAs)
    }

    const onReset = () => {
        const { newAs, href } = makeDisplayUrls({}, baseUrl, as)
        router.push(href, newAs)
    }

    return {
        apply: onApply,
        reset: onReset,
    }
}
