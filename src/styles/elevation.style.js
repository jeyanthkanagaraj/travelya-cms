
/**
 * Elevation Mixin - Valid elevations are from 0 - 24
 * JS implementation from material design adapted from
 * https://github.com/material-components/material-components-web/blob/master/packages/mdc-elevation/_variables.scss
 */
const umbraColor = 'rgba(0, 0, 0, 0.24)'
const penumbraColor = 'rgba(0, 0, 0, 0.2)'
const ambientColor = 'rgba(0, 0, 0, 0.16)'

const umbra = [
    '.0rem .0rem .0rem .0rem',
    '.0rem .2rem .1rem -.1rem',
    '.0rem .3rem .1rem -.2rem',
    '.0rem .3rem .3rem -.2rem',
    '.0rem .2rem .4rem -.1rem',
    '.0rem .3rem .5rem -.1rem',
    '.0rem .3rem .5rem -.1rem',
    '.0rem .4rem .5rem -.2rem',
    '.0rem .5rem .5rem -.3rem',
    '.0rem .5rem .6rem -.3rem',
    '.0rem .6rem .6rem -.3rem',
    '.0rem .6rem .7rem -.4rem',
    '.0rem .7rem .8rem -.4rem',
    '.0rem .7rem .8rem -.4rem',
    '.0rem .7rem .9rem -.4rem',
    '.0rem .8rem .9rem -.5rem',
    '.0rem .8rem 1.0rem -.5rem',
    '.0rem .8rem 1.1rem -.5rem',
    '.0rem .9rem 1.1rem -.5rem',
    '.0rem .9rem 1.2rem -.6rem',
    '.0rem 1.0rem 1.3rem -.6rem',
    '.0rem 1.0rem 1.3rem -.6rem',
    '.0rem 1.0rem 1.4rem -.6rem',
    '.0rem 1.1rem 1.4rem -.7rem',
    '.0rem 1.1rem 1.5rem -.7rem'
]

const penumbra = [
    '.0rem .0rem .0rem .0rem',
    '.0rem .1rem .1rem .0rem',
    '.0rem .2rem .2rem .0rem',
    '.0rem .3rem .4rem .0rem',
    '.0rem .4rem .5rem .0rem',
    '.0rem .5rem .8rem .0rem',
    '.0rem .6rem 1.0rem .0rem',
    '.0rem .7rem 1.0rem .1rem',
    '.0rem .8rem 1.0rem .1rem',
    '.0rem .9rem 1.2rem .1rem',
    '.0rem 1.0rem 1.4rem .1rem',
    '.0rem 1.1rem 1.5rem .1rem',
    '.0rem 1.2rem 1.7rem .2rem',
    '.0rem 1.3rem 1.9rem .2rem',
    '.0rem 1.4rem 2.1rem .2rem',
    '.0rem 1.5rem 2.2rem .2rem',
    '.0rem 1.6rem 2.4rem .2rem',
    '.0rem 1.7rem 2.6rem .2rem',
    '.0rem 1.8rem 2.8rem .2rem',
    '.0rem 1.9rem 2.9rem .2rem',
    '.0rem 2.0rem 3.1rem .3rem',
    '.0rem 2.1rem 3.3rem .3rem',
    '.0rem 2.2rem 3.5rem .3rem',
    '.0rem 2.3rem 3.6rem .3rem',
    '.0rem 2.4rem 3.8rem .3rem'
]

const ambient = [
    '.0rem .0rem .0rem .0rem',
    '.0rem .1rem .3rem .0rem',
    '.0rem .1rem .5rem .0rem',
    '.0rem .1rem .8rem .0rem',
    '.0rem .1rem 1.0rem .0rem',
    '.0rem .1rem 1.4rem .0rem',
    '.0rem .1rem 1.8rem .0rem',
    '.0rem .2rem 1.6rem .1rem',
    '.0rem .3rem 1.4rem .2rem',
    '.0rem .3rem 1.6rem .2rem',
    '.0rem .4rem 1.8rem .3rem',
    '.0rem .4rem 2.0rem .3rem',
    '.0rem .5rem 2.2rem .4rem',
    '.0rem .5rem 2.4rem .4rem',
    '.0rem .5rem 2.6rem .4rem',
    '.0rem .6rem 2.8rem .5rem',
    '.0rem .6rem 3.0rem .5rem',
    '.0rem .6rem 3.2rem .5rem',
    '.0rem .7rem 3.4rem .6rem',
    '.0rem .7rem 3.6rem .6rem',
    '.0rem .8rem 3.8rem .7rem',
    '.0rem .8rem 4.0rem .7rem',
    '.0rem .8rem 4.2rem .7rem',
    '.0rem .9rem 4.4rem .8rem',
    '.0rem .9rem 4.6rem .8rem'
]

export const makeElevation = z => ` 
    ${umbra[z]} ${umbraColor},
    ${penumbra[z]} ${penumbraColor},
    ${ambient[z]} ${ambientColor};
`
