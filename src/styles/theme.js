import { red, blue } from '@material-ui/core/colors'
import { makeMediaQuery } from 'lib/makeMediaQuery'

export const defaultTheme = {
    palette: {
        primary: { main: blue[800] },
        secondary: { main: '#e85b24' },
        success: { main: '#b5c034' },
        info: { main: '#fff' },
        error: {
            main: red[500]
        },
        common: {
            background: '#212121',
            gray: '#eaeaea',
            white: '#fff',
            black: '#000000',
        },
        tonalOffset: 0.2,
        contrastThreshold: 3,

    },
    mq: makeMediaQuery()
}
