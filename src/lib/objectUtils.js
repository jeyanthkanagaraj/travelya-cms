import get from 'lodash/get'

/**
 *
 * @param {Object} object takes and object and removes falsy values
 */
export const cleanObject = object => Object.keys(object).reduce((acc, key) => {
    const current = object[key]
    current && (acc[key] = current)
    return acc
}, {})

export const groupBy = (items, category) => items.reduce((acc, current) => {
    const key = get(current, category)
    acc[key] ? acc[key].push(current) : (acc[key] = [current])
    return acc
}, {})
