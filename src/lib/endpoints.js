//import getConfig from 'next/config'
//const ENV = getConfig().publicRuntimeConfig
//const BASE_URL = `${ENV.API_PROTOCOL}${ENV.API_HOST}`
//const BASE_URL = 'http://localhost:8080'
const BASE_URL = 'https://travelaya-api.uc.r.appspot.com'

export const endpoints = {
    test: {
        methods: ['post'],
        url: `${BASE_URL}/user`
        //url: `${BASE_URL}${ENV.API_ENDPOINTS.TEST}`
    },
    users: {
        methods: ['get', 'post'],
        url: `${BASE_URL}/user`
    },
    flights: {
        methods: ['get'],
        url: `${BASE_URL}/flights`
    },
    airport: {
        methods: ['get', 'patch', 'delete'],
        url: `${BASE_URL}/airports/:id`
    },
    airports: {
        methods: ['get', 'post'],
        url: `${BASE_URL}/airports`
    },
    destinations: {
        methods: ['get', 'post'],
        url: `${BASE_URL}/destinations`
    },
    destination: {
        methods: ['get', 'post', 'patch'],
        url: `${BASE_URL}/destinations/:code`
    },
    countries: {
        methods: ['get'],
        url: `${BASE_URL}/countries`
    },
    country: {
        methods: ['get', 'patch'],
        url: `${BASE_URL}/countries/:code`
    }
}
