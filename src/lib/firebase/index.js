import firebase from "firebase/app";
import "firebase/auth";
import "firebase/storage";
import getConfig from "next/config";
const appConfig = getConfig().publicRuntimeConfig;
const config = {
  apiKey: appConfig.GOOGLE.API_KEY,
  authDomain: appConfig.GOOGLE.AUTH_DOMAIN,
  databaseURL: appConfig.DATABASE_URL,
  projectId: appConfig.PROJECT_ID,
  storageBucket: appConfig.STORAGE_BUCKET,
  messagingSenderId: appConfig.MESSAGING_SENDER_ID,
  appId: appConfig.APP_ID,
};
if (!firebase.apps.length) {
  firebase.initializeApp(config);
}
const auth = firebase.auth();
const storage = firebase.storage();
export { auth, firebase, storage };
