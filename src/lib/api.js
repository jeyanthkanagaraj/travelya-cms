import { endpoints } from 'lib/endpoints'
import { http } from 'lib/http'

const createIntegrationMethod = (url, method) => {
    if (/[\/\/].+:[^0-9]/.test(url)) {
        return (params = {}, config) => {
            const urlWithParams = Object.entries(params).reduce((u, entry) => u.replace(`:${entry[0]}`, entry[1]), url)
            return http.request(urlWithParams, method, config)
        }
    }

    return config => http.request(url, method, config)
}

/**
 * @description this function creates the integration endpoints for the api
 * based on the endpoint config
 */
export const api = Object.entries(endpoints).reduce((accumulator, entry) => {
    const [key, integration] = entry
    accumulator[key] = integration.methods.reduce((acc, method) => {
        acc[method] = createIntegrationMethod(integration.url, method)
        return acc
    }, {})

    return accumulator
}, {})
