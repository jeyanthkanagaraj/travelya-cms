export const DEFAULT_FILTERS = {
    page: 1,
    per_page: 10,
    _sort: 'id:asc',
}
