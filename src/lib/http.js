import axios from 'axios'
import cookie from 'js-cookie'

class Http {
    constructor () {
        this.instance = axios.create()
        this.setContentType()
    }

    setContentType () {
        this.instance.defaults.headers.patch['Content-Type'] = 'application/json, text/plain, */*'
        this.instance.defaults.headers.post['Content-Type'] = 'application/json, text/plain, */*'
        this.instance.defaults.headers.put['Content-Type'] = 'application/json, text/plain, */*'
    }

    setToken (token) {
        this.instance.defaults.headers.common['authorization'] = `Bearer ${token}`
    }

    clearToken () {
        delete this.instance.defaults.headers.common['authorization']
    }

    async request (url, method, config = {}) {
        if (!this.instance.defaults.headers.common.authorization) {
            this.setToken(cookie.get('userToken'))
        }
        return this.instance.request({
            url,
            method,
            ...config,
        })
    }
}

const http = new Http()
export { http }
