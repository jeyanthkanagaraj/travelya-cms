import router from 'next/router'
import React from 'react'
import Button from 'components/Buttons/Button'
import { api } from 'lib/api'
import { auth } from 'lib/firebase'

const Dashboard = () => {
    const handleLogout = () => {
        auth.signOut().then(function () {
            alert('Logout successful')
            router.push('/')
        }).catch(function (error) {
            alert('Oops something went wrong check your console')
            console.log(err)
        })
    }

    const handleRequest = async () => {
        const foo = await api.test.post()
        console.log(foo)
    }

    console.log(auth.currentUser)

    return (
        <div>
            <h1>Dashboard  Page</h1>
            <p>You can't go into this page if you are not authenticated.</p>
            <Button onClick={handleRequest}>Test Request</Button>
            <Button onClick={handleLogout}>Logout</Button>
        </div>
    )
}
export default Dashboard
