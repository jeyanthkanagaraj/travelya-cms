import Checkbox from '@material-ui/core/Checkbox'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import moment from 'moment'
import React, { Fragment, useEffect, useState } from 'react'
import Datatable from 'components/Datatable/Datatable'
import LinkCell from 'components/Datatable/LinkCell'
import PaginationWrapper from 'components/PaginationWrapper/PaginationWrapper'
import Section from 'components/Section/Section'
import { useNotification } from 'context/NotificationContext'
import { useUI } from 'context/UIContext'
import { api } from 'lib/api'
import { DEFAULT_FILTERS } from 'lib/constants'

const Country = ({ country, destinations, query }) => {
    const ui = useUI()
    const notification = useNotification()

    const [des, setDes] = useState()

    useEffect(() => {
        ui.setPageTitle('Country: ' + country.name)
        return () => ui.setPageTitle('')
    }, [])

    useEffect(() => {
        setDes(destinations.destinations)
    }, [destinations])

    const toggleChecked = async (destination, checked, index) => {
        const newDes = [...des]
        newDes[index].enabled = checked
        setDes(newDes)
        try {
            await api.destination.patch({ code: destination.code }, { data: { ...destination, enabled: checked } })
        }
        catch (error) {
            notification.dispatch('Something went wrong', 'success')
        }
        notification.dispatch(`Destination successfully ${checked ? 'enabled' : 'disabled'}`, 'success')
    }

    const columns = [
        {
            field: 'enabled',
            header: 'Enabled',
            padding: 'checkbox',
            cell: (row, index) => <Checkbox checked={row.enabled} onChange={e => toggleChecked(row, e.target.checked, index)} />
        },
        {
            field: 'code',
            header: 'Code',
            cell: row => <LinkCell baseUrl="destination" row={row} id={row.code}>{row.code}</LinkCell>
        },
        {
            field: 'name',
            header: 'Name'
        }
    ]

    return (
        <Fragment>
            <Section title="Basic Info">
                <List>
                    <ListItem>
                        <ListItemText primary={`${country.name} (${country.code})`} secondary="Name" />
                    </ListItem>
                    <ListItem>
                        <ListItemText primary={moment(country.updatedAt).fromNow()} secondary="Last Updated" />
                    </ListItem>
                </List>
            </Section>
            <PaginationWrapper
                as={`/country/${country.code}`}
                baseUrl={'country/[id]'}
                query={query}
                count={destinations.count}>
                <Datatable
                    columns={columns}
                    data={des}
                    heading="Destinations" />
            </PaginationWrapper>
        </Fragment>
    )
}

Country.getInitialProps = async ({ query }) => {
    const destinationParams = {
        countryCode: query.id,
        ...DEFAULT_FILTERS,
        ...query
    }
    try {
        const [country, destinations] = await Promise.all([
            api.country.get({ code: query.id }),
            getDestinations(destinationParams)
        ])
        return {
            country: country.data,
            destinations: destinations.data,
            query: destinationParams
        }
    }
    catch (error) {
        console.error(error)
        return {}
    }
}

const getDestinations = async params => {
    const d = await api.destinations.get({ params })
    return d
}

export default Country
