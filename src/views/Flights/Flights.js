import Tooltip from '@material-ui/core/Tooltip'
import React from 'react'
import AddButton from 'components/Buttons/AddButton'
import Datatable from 'components/Datatable/Datatable'
import LinkCell from 'components/Datatable/LinkCell'
import PaginationWrapper from 'components/PaginationWrapper/PaginationWrapper'
import { api } from 'lib/api'
import { DEFAULT_FILTERS } from 'lib/constants'

const Flights = ({ flights, query }) => {
    console.log(flights)

    const columns = [
        {
            field: 'id',
            header: 'ID',
            cell: row => <LinkCell baseUrl="flight" row={row}>{row.id}</LinkCell>
        }
    ]

    const sortOptions = []

    return (
        <PaginationWrapper
            baseUrl="flights"
            items={flights}
            query={query}
            options={sortOptions}
            renderHeader={() => (
                <Tooltip placement="right" title="Create new Flight">
                    <AddButton />
                </Tooltip>
            )}>
            <Datatable
                columns={columns}
                data={flights}
                heading="Flights" />
        </PaginationWrapper>
    )
}

Flights.getInitialProps = async ({ query }) => {
    const params = {
        ...DEFAULT_FILTERS,
        ...query
    }

    try {
        const flights = await api.flights.get({ params })
        return {
            flights: flights.data,
            query: params
        }
    }
    catch (error) {
        console.error(error)
        return {}
    }
}

export default Flights
