import Checkbox from '@material-ui/core/Checkbox'
import React, { useState, useEffect } from 'react'
import Datatable from 'components/Datatable/Datatable'
import LinkCell from 'components/Datatable/LinkCell'
import PaginationWrapper from 'components/PaginationWrapper/PaginationWrapper'
import { useNotification } from 'context/NotificationContext'
import { api } from 'lib/api'
import { DEFAULT_FILTERS } from 'lib/constants'

const Countries = ({ countries, query }) => {
    const notification = useNotification()
    const [cons, setCons] = useState(countries.countries)

    useEffect(() => {
        setCons(countries.countries)
    }, [countries])

    const toggleChecked = async (country, checked, index) => {
        const newCons = [...cons]
        newCons[index].enabled = checked
        setCons(newCons)
        try {
            await api.country.patch({ code: country.code }, { data: { ...country, enabled: checked } })
        }
        catch (error) {
            notification.dispatch('Something went wrong', 'success')
        }
        notification.dispatch(`Country successfully ${checked ? 'enabled' : 'disabled'}`, 'success')
    }

    const columns = [
        {
            field: 'enabled',
            header: 'Enabled',
            padding: 'checkbox',
            cell: (row, index) => <Checkbox checked={row.enabled} onChange={e => toggleChecked(row, e.target.checked, index)} />
        },
        {
            field: 'code',
            header: 'Code',
            cell: row => <LinkCell baseUrl="country" row={row} id={row.code}>{row.code}</LinkCell>
        },
        {
            field: 'name',
            header: 'Name',
            cell: row => row.name
        },
        {
            field: 'isoCode',
            header: 'ISO Code',
        },
    ]

    const sortOptions = []

    return (
        <PaginationWrapper
            baseUrl="countries"
            count={countries.count}
            query={query}
            options={sortOptions}>
            <Datatable
                columns={columns}
                data={cons}
                heading="Countries" />
        </PaginationWrapper>
    )
}

Countries.getInitialProps = async ({ query }) => {
    const params = {
        ...DEFAULT_FILTERS,
        ...query
    }

    try {
        const countries = await api.countries.get({ params })
        return {
            countries: countries.data,
            query: params
        }
    }
    catch (error) {
        console.error(error)
        return { countries: {} }
    }
}

export default Countries
