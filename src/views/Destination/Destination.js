import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemText from '@material-ui/core/ListItemText'
import { makeStyles } from '@material-ui/core/styles'
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight'
import moment from 'moment'
import { useRouter } from 'next/router'
import React, { Fragment, useEffect, useState } from 'react'
import Section from 'components/Section/Section'
import { useUI } from 'context/UIContext'
import { api } from 'lib/api'
import { Typography, Button } from '@material-ui/core'
import{ storage } from 'lib/firebase'

const Destination = ({ destination }) => {
    const ui = useUI()
    const classes = useStyles()
    const router = useRouter()
    const [Image, setImage] = useState(null)
    const [ImageURL, setImageURL] = useState(null)
    const onFileChange = (e) => {
        if(e.target.files[0]) {
            setImage(e.target.files[0])
        }
    }

    const handleClick = () => {
        const imageUpload = storage.ref(`images/${Image.name}`).put(Image)
        imageUpload.on('state_changed', 
        (snapshot) => { console.log(snapshot)}, 
        (error) => {console.log(error)}, 
        () => storage.ref('images').child(image.name).getDownloadURL().then(resp => console.log(resp)))
    }
    useEffect(() => {
        ui.setPageTitle('Destination: ' + destination.name)
        return () => ui.setPageTitle('')
    }, [])
    console.log(storage)
    return (
        <Fragment>
            <Section title="Basic Info">
                <List>
                    <ListItem divider>
                        <ListItemText primary={`${destination.name} (${destination.code})`} secondary="Name" />
                    </ListItem>
                    <ListItem divider button onClick={() => router.push('/country/[id]', `/country/${destination.country.code}`)}>
                        <ListItemText primary={`${destination.country.name} (${destination.country.code})`} secondary="Country" />
                        <KeyboardArrowRight />
                    </ListItem>
                    <ListItem divider>
                        <ListItemText primary={moment(destination.updatedAt).fromNow()} secondary="Last Updated" />
                    </ListItem>
                </List>
            </Section>
            <Section title="Hotels" >
                <List>
                    <ListItem>
                        <ListItemText primary="Coming Soon" />
                    </ListItem>
                </List>
            </Section>
            <Section title="Images" button={<label htmlFor="contained-button-file"><Typography variant="contained" color="primary" className={classes.uploadButton}>Add Image</Typography></label>}>
                <input
                accept="image/*"
                className={classes.inputUpload}
                id="contained-button-file"
                type="file"
                onChange={onFileChange}
                />
                <List>
                    <ListItem>
                        <ListItemText primary="Coming Soon" />
                        <Button onClick={handleClick}>upload</Button>
                    </ListItem>
                </List>
            </Section>
        </Fragment>
    )
}

Destination.getInitialProps = async ({ query }) => {
    try {
        const destination = await api.destination.get({ code: query.id })
        return {
            destination: destination.data
        }
    }
    catch (error) {
        console.error(error)
        return {}
    }
}

const useStyles = makeStyles(theme => ({
    inputUpload: {
        display: 'none',
    },
    uploadButton: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.info.main,
        padding: '0.5rem 1rem',
        borderRadius: '0.2rem'
    }
}))

export default Destination
