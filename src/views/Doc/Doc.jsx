import { ServerStyleSheets } from '@material-ui/core/styles'
import Document, {
    Html,
    Head,
    Main,
    NextScript,
} from 'next/document'
import React from 'react'
import { resetServerContext } from 'react-beautiful-dnd'

export class Doc extends Document {
    static async getInitialProps (ctx) {
        // Render app and page and get the context of the page with collected side effects.
        const sheets = new ServerStyleSheets()
        const originalRenderPage = ctx.renderPage
        resetServerContext()
        ctx.renderPage = () =>
            originalRenderPage({
                enhanceApp: App => props => sheets.collect(<App {...props} />),
            })

        const initialProps = await Document.getInitialProps(ctx)

        return {
            ...initialProps,
            // Styles fragment is rendered after the app and page rendering finish.
            styles: [
                ...React.Children.toArray(initialProps.styles),
                sheets.getStyleElement()
            ],
        }
    }

    render () {
        return (
            <Html>
                <Head>
                    <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width, shrink-to-fit=no" />
                    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        )
    }
}
