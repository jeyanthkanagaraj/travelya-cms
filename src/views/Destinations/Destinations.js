import Checkbox from '@material-ui/core/Checkbox'
import React, { useEffect, useState } from 'react'
import Datatable from 'components/Datatable/Datatable'
import LinkCell from 'components/Datatable/LinkCell'
import PaginationWrapper from 'components/PaginationWrapper/PaginationWrapper'
import { useNotification } from 'context/NotificationContext'
import { api } from 'lib/api'
import { DEFAULT_FILTERS } from 'lib/constants'

const Destinations = ({ destinations, query }) => {
    const notification = useNotification()
    const [des, setDes] = useState()

    useEffect(() => {
        setDes(destinations.destinations)
    }, [destinations])

    const toggleChecked = async (destination, checked, index) => {
        const newDes = [...des]
        newDes[index].enabled = checked
        setDes(newDes)
        try {
            await api.destination.patch({ code: destination.code }, { data: { ...destination, enabled: checked } })
        }
        catch (error) {
            notification.dispatch('Something went wrong', 'success')
        }
        notification.dispatch(`Destination successfully ${checked ? 'enabled' : 'disabled'}`, 'success')
    }

    const columns = [
        {
            field: 'enabled',
            header: 'Enabled',
            padding: 'checkbox',
            cell: (row, index) => <Checkbox checked={row.enabled} onChange={e => toggleChecked(row, e.target.checked, index)} />
        },
        {
            field: 'code',
            header: 'Code',
            cell: row => <LinkCell baseUrl="destination" row={row} id={row.code}>{row.code}</LinkCell>
        },
        {
            field: 'name',
            header: 'Name',
            cell: row => row.name
        },
        {
            field: 'country',
            header: 'Country',
            cell: row => {
                return row.country?.name
            }
        },
    ]

    const sortOptions = []

    return (
        <PaginationWrapper
            baseUrl="destinations"
            count={destinations.count}
            query={query}
            options={sortOptions}>
            <Datatable
                columns={columns}
                data={des}
                heading="Destinations" />
        </PaginationWrapper>
    )
}

Destinations.getInitialProps = async ({ query }) => {
    const params = {
        ...DEFAULT_FILTERS,
        ...query
    }

    try {
        const destinations = await api.destinations.get({ params })
        return {
            destinations: destinations.data,
            query: params
        }
    }
    catch (error) {
        console.error(error)
        return {}
    }
}

export default Destinations
