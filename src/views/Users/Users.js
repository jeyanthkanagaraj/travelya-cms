import moment from 'moment'
import React from 'react'
import Datatable from 'components/Datatable/Datatable'
import LinkCell from 'components/Datatable/LinkCell'
import PaginationWrapper from 'components/PaginationWrapper/PaginationWrapper'
import { api } from 'lib/api'
import { DEFAULT_FILTERS } from 'lib/constants'

const Users = ({ users, query }) => {
    const columns = [
        {
            field: 'gId',
            header: 'ID',
            cell: row => <LinkCell baseUrl="user" row={row}>{row.gId}</LinkCell>
        }, {
            field: 'name',
            header: 'Name',
            cell: row => row.firstName + ' ' + row.lastName
        },
        {
            field: 'email',
            header: 'Email',
        },
        {
            field: 'createdAt',
            header: 'Sign Up Date',
            cell: row => {
                return moment(row.createdAt).format('MM/DD/YYYY hh:mm a')
            }
        }

    ]

    const sortOptions = []

    return (
        <PaginationWrapper
            baseUrl="flights"
            items={users}
            query={query}
            options={sortOptions}>
            <Datatable
                columns={columns}
                data={users}
                heading="Users" />
        </PaginationWrapper>
    )
}

Users.getInitialProps = async ({ query }) => {
    const params = {
        ...DEFAULT_FILTERS,
        ...query
    }

    try {
        const users = await api.users.get({ params })
        return {
            users: users.data,
            query: params
        }
    }
    catch (error) {
        console.error(error)
        return {}
    }
}

export default Users
