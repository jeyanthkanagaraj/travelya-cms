import Button from '@material-ui/core/Button'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import IconDelete from '@material-ui/icons/Delete'
import IconEdit from '@material-ui/icons/Edit'
import LocalAirport from '@material-ui/icons/LocalAirport'
import LocationOn from '@material-ui/icons/LocationOn'
import Public from '@material-ui/icons/Public'
import { useRouter } from 'next/router'
import React, { Fragment, useState } from 'react'
import AirportDialog from 'components/Dialogs/Airport'
import ConfirmationDialog from 'components/Dialogs/ConfirmationDialog'
import { api } from 'lib/api'

const Airport = props => {
    const classes = useStyles()
    const router = useRouter()
    const [airport, setAirport] = useState(props.airport)
    const [editDialog, setEditDialog] = useState(false)
    const [deleteDialog, setDeleteDialog] = useState(false)

    const handleClose = () => setEditDialog(false)
    const onEditSuccess = patchedAirport => {
        setAirport(patchedAirport)
        setEditDialog(false)
    }

    const handleDelete = async () => {
        const { id } = airport
        try {
            await api.airport.delete({ id })
            setDeleteDialog(false)
            return router.push('/airports')
        }
        catch (error) {
            console.error(error)
        }
    }

    return (
        <Fragment>
            <Typography variant="h5" gutterBottom>
                Airport Detail
            </Typography>
            <Button
                color="primary"
                variant="contained"
                startIcon={<IconEdit />}
                className={classes.button}
                onClick={() => setEditDialog(true)}>
                Edit
            </Button>
            <Button
                color="secondary"
                variant="contained"
                startIcon={<IconDelete />}
                className={classes.button}
                onClick={() => setDeleteDialog(true)}>
                Delete
            </Button>
            <List>
                <ListItem>
                    <ListItemIcon>
                        <LocalAirport />
                    </ListItemIcon>
                    <ListItemText primary="Name" secondary={`${airport.name} (${airport.code})`} />
                </ListItem>
                <ListItem>
                    <ListItemIcon>
                        <LocationOn />
                    </ListItemIcon>
                    <ListItemText
                        primary="Address"
                        secondary={`${airport.address}, ${airport.city} ${airport.state} ${airport.zip}`} />
                </ListItem>
                <ListItem>
                    <ListItemIcon>
                        <Public />
                    </ListItemIcon>
                    <ListItemText primary="Country" secondary={airport.country} />
                </ListItem>
            </List>
            <AirportDialog
                editAirport={airport}
                open={editDialog}
                close={handleClose}
                onSuccess={onEditSuccess} />
            <ConfirmationDialog
                open={deleteDialog}
                options={{
                    title: 'Delete Airport',
                    description: `Are you sure you want to permanently delete ${airport.name}?`,
                    confirmationText: 'Confirm',
                    cancellationText: 'Cancel'
                }}
                onCancel={() => setDeleteDialog(false)}
                onConfirm={handleDelete} />
        </Fragment>
    )
}

Airport.getInitialProps = async ({ query }) => {
    const { id } = query

    try {
        const airport = await api.airport.get({ id })
        return { airport: airport.data }
    }
    catch (error) {
        console.error(error)
        return {}
    }

}

const useStyles = makeStyles(theme => ({
    button: {
        marginRight: theme.spacing(2),
    }
}))

export default Airport
