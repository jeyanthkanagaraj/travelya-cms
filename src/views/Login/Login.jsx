import Divider from '@material-ui/core/Divider'
import Grid from '@material-ui/core/Grid'
import { makeStyles } from '@material-ui/core/styles'
import { useRouter } from 'next/router'
import React, { useState } from 'react'
import Button from 'components/Buttons/Button'
import Form, { actions } from 'components/Form'
import ErrorText from 'components/Form/ErrorText/ErrorText'
import TextField from 'components/Form/TextField'
import { api } from 'lib/api'
import { auth, firebase } from 'lib/firebase'

const Login = () => {
    const [loading, setLoading] = useState(false)
    const classes = useStyles()
    const router = useRouter()

    const handleSubmit = async (state, dispatch) => {
        setLoading(true)
        auth.signInWithEmailAndPassword(state.values.identifier, state.values.password)
            .then(() => {
                router.push('/dashboard')
            })
            .catch(function (error) {
                if (error.response?.status === 400) {
                    dispatch({
                        type: actions.setErrors,
                        payload: { loginError: 'Username, email or password incorrect.' }
                    })
                }
                return setLoading(false)
            })
    }

    const handleSignInWithGoogle = () => {
        var provider = new firebase.auth.GoogleAuthProvider()
        provider.addScope('https://www.googleapis.com/auth/contacts.readonly')
        auth.signInWithPopup(provider)
            .then((result) => {
                // insert user into database
                const user = {
                    gId: result.user.uid,
                    email: result.user.email,
                    picture: result.additionalUserInfo.profile.picture,
                    firstName: result.additionalUserInfo.profile.given_name,
                    lastName: result.additionalUserInfo.profile.family_name
                }
                api.users.post({ data: user })
                router.push('/dashboard')
            })
            .catch(err => {
                alert('Oops something went wrong check your console')
                console.log(err)
            })
    }

    return (
        <Grid container direction="column" justify="center" className={classes.root}>
            <Form onSubmit={handleSubmit} className={classes.form}>
                <Grid container>
                    <Grid container>
                        <Grid item xs={12} className={classes.textField}>
                            <TextField
                                dark
                                label="USERNAME/EMAIL"
                                name="identifier"
                                validation={['required']} />
                        </Grid>
                        <Grid item xs={12} className={classes.textField}>
                            <TextField
                                dark
                                label="PASSWORD"
                                type="password"
                                name="password"
                                validation={['required']} />
                        </Grid>
                    </Grid>
                    <Grid container className={classes.container}>
                        <Button
                            color="primary"
                            className={classes.button}
                            disabled={loading}
                            fullWidth
                            loading={loading}
                            type="submit">
                            Sign In
                        </Button>
                        <Button type="button" variant="text" onClick={() => router.push('/sign-up')} fullWidth className={classes.button}>Sign Up</Button>
                        <Divider className={classes.divider} />
                        <Button onClick={handleSignInWithGoogle} fullWidth type="button">
                            <img
                                className={classes.google}
                                src="https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/1200px-Google_%22G%22_Logo.svg.png" />
                            Sign In With Google
                        </Button>
                    </Grid>
                    <ErrorText name="loginError" />
                </Grid>
            </Form>
        </Grid>
    )
}
export default Login

const useStyles = makeStyles(theme => {
    return {
        root: {
            display: 'flex',
            alignItems: 'center',
            height: '100vh',
            width: '100%',
            justifyContent: 'center',
        },
        button: {
            marginBottom: theme.spacing(2),
            backgroundColor: 'none'
        },
        container: {
            '& > div': {
                width: '100%',
            }
        },
        form: {
            padding: theme.spacing(2),
        },
        textField: {
            marginBottom: theme.spacing(1),
        },
        image: {
            maxWidth: '50vw',
            height: theme.spacing(50)
        },
        google: {
            height: '2rem',
            marginRight: '1rem'
        },
        divider: {
            margin: '0 0 1rem 0',
            width: '100%',
        }
    }
})
