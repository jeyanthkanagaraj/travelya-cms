import Tooltip from '@material-ui/core/Tooltip'
import React, { useState } from 'react'
import AddButton from 'components/Buttons/AddButton'
import Datatable from 'components/Datatable/Datatable'
import LinkCell from 'components/Datatable/LinkCell'
import AirportDialog from 'components/Dialogs/Airport'
import PaginationWrapper from 'components/PaginationWrapper/PaginationWrapper'
import { api } from 'lib/api'
import { DEFAULT_FILTERS } from 'lib/constants'

const Airports = props => {
    const [airports, setAirports] = useState(props.airports)
    const [query, setQuery] = useState(props.query)
    const [showDialog, setShowDialog] = useState(false)

    const columns = [
        {
            field: 'id',
            header: 'ID',
            cell: row => <LinkCell baseUrl="airport" row={row}>{row.id}</LinkCell>
        },
        {
            field: 'code',
            header: 'Code',
            cell: row => row.code
        },
        {
            field: 'name',
            header: 'Name',
            cell: row => row.name
        },
        {
            field: 'address',
            header: 'Address',
            cell: row => row.address
        },
        {
            field: 'city',
            header: 'City',
            cell: row => row.city
        },
        {
            field: 'state',
            header: 'State',
            cell: row => row.state
        },
        {
            field: 'zip',
            header: 'Zip',
            cell: row => row.zip
        },
        {
            field: 'country',
            header: 'Country',
            cell: row => row.country
        },
    ]

    const sortOptions = []

    const handleClose = () => {
        setShowDialog(false)
    }

    const onSuccess = async () => {
        const data = await fetchAirportData({ ...DEFAULT_FILTERS })
        setAirports(data.airports)
        setQuery(data.query)
        handleClose()
    }

    return (
        <PaginationWrapper
            baseUrl="airports"
            items={airports}
            query={query}
            options={sortOptions}
            renderHeader={() => (
                <Tooltip placement="right" title="Create new Airport">
                    <AddButton onClick={() => setShowDialog(true)} />
                </Tooltip>
            )}>
            <Datatable
                columns={columns}
                data={airports}
                addAction={() => setShowDialog(true)}
                heading="Airports" />
            <AirportDialog
                open={showDialog}
                close={handleClose}
                onSuccess={onSuccess} />
        </PaginationWrapper>
    )
}

Airports.getInitialProps = async ({ query }) => {
    const params = {
        ...DEFAULT_FILTERS,
        ...query
    }
    return fetchAirportData(params)
}

async function fetchAirportData (params) {
    try {
        const airports = await api.airports.get({ params })
        return {
            airports: airports.data,
            query: params
        }
    }
    catch (error) {
        console.error(error)
        return {}
    }
}

export default Airports
