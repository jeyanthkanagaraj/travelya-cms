import DateFnsUtils from '@date-io/date-fns'
import CssBaseline from '@material-ui/core/CssBaseline'
import { ThemeProvider as MuiThemeProvider } from '@material-ui/core/styles'
import { createMuiTheme } from '@material-ui/core/styles'
import { MuiPickersUtilsProvider } from '@material-ui/pickers'
import nextCookie from 'next-cookies'
import App from 'next/app'
import React from 'react'
import { Layout } from 'components/Layout/Layout'
import NProgressContainer from 'components/NProgressContainer/NProgressContainer'
import { ConfirmContextProvider } from 'context/ConfirmContext'
import { NotificationContextProvider } from 'context/NotificationContext'
import { UIContextProvider } from 'context/UIContext'
import { http } from 'lib/http'
import { makeElevation } from 'styles/elevation.style'
import { defaultTheme } from 'styles/theme'

const theme = createMuiTheme(defaultTheme, { elevation: z => makeElevation(z) })

class Application extends App {
    static async getInitialProps ({ Component, ctx }) {
        try {
            if (typeof window === 'undefined') {
                // sets the http token from cookie so that we can load data before initial page load
                // and before firebase is setup client-side
                const { userToken } = nextCookie(ctx)

                // if a token was found, try to do SSA
                if (userToken) {
                    http.setToken(userToken)

                }
            }
            let pageProps = {}
            if (Component.getInitialProps) {
                pageProps = await Component.getInitialProps(ctx)
            }
            return {
                pageProps,
            }
        }
        catch (e) {
            // let exceptions fail silently
            // could be invalid token, just let client-side deal with that
        }
    }

    componentDidMount () {
        const style = document.getElementById('jss-server-side')
        if (style) {
            style.parentElement.removeChild(style)
        }
    }

    render () {
        const { Component, pageProps } = this.props

        return (
            <div style={{ backgroundColor: '#eee', minHeight: '100vh' }}>
                <MuiThemeProvider theme={theme}>
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <CssBaseline />
                        <NProgressContainer />
                        <NotificationContextProvider>
                            <UIContextProvider>
                                <ConfirmContextProvider>
                                    <Layout>
                                        <Component {...pageProps} />
                                    </Layout>
                                </ConfirmContextProvider>
                            </UIContextProvider>
                        </NotificationContextProvider>
                    </MuiPickersUtilsProvider>
                </MuiThemeProvider>
            </div>
        )
    }
}

export default Application
