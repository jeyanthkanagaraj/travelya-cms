
import {makeStyles} from '@material-ui/core/styles'
import MuiTextField from '@material-ui/core/TextField'
import Autocomplete from '@material-ui/lab/Autocomplete'
import React from 'react'

const useStyles = makeStyles(theme => ({
    autoComplete: {
        background: theme.palette.common.light
    }
}))

const FilterSort = ({options, selected, onSelect}) => {
    const selectedOption = options.find(option => option.value === selected)
    const classes = useStyles()

    return (
        <Autocomplete
            className={classes.autoComplete}
            options={options}
            getOptionLabel={option => option.name}
            style={{width: 300}}
            onChange={(e, select) => onSelect(select)}
            value={selectedOption}
            renderInput={params => (
                <MuiTextField
                    {...params}
                    label="Sort"
                    variant="outlined"
                    fullWidth />
            )} />
    )
}

export default FilterSort
