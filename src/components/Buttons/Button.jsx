import MuiButton from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'
import { makeStyles } from '@material-ui/core/styles'
import clsx from 'clsx'
import React from 'react'

const useStyles = makeStyles(theme => ({
    button: {
        minWidth: theme.spacing(24),
        padding: theme.spacing(2),
        [theme.breakpoints.down('sm')]: {
            width: '100%',
        },
    },
    buttonProgress: {
        position: 'absolute',
        top: '50%',
        left: '50%',
        marginTop: -12,
        marginLeft: -12,
    },
    wrapper: {
        position: 'relative',
        [theme.breakpoints.down('sm')]: {
            width: '100%',
        },
    }
}))

const Button = ({
    children,
    className,
    color,
    dark,
    loading,
    variant,
    ...extra
}) => {
    const classes = useStyles({ dark })

    return (
        <div className={classes.wrapper}>
            <MuiButton
                {...extra}
                className={clsx(classes.button, { [className]: !!className })}
                variant={variant || 'contained'}
                color={color}>
                {children}
            </MuiButton>
            {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
        </div>
    )
}
Button.defaultProps = {
    color: 'default',
    type: 'submit'
}

export default Button
