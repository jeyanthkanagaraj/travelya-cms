import Fab from '@material-ui/core/Fab'
import IconAdd from '@material-ui/icons/Add'
import IconRemove from '@material-ui/icons/Remove'
import React from 'react'

const AddButton = React.forwardRef(({ negative, ...props }, ref) => {
    return (
        <Fab
            aria-controls="menu"
            aria-label="add new menu item"
            aria-haspopup="true"
            color="primary"
            size="medium"
            ref={ref}
            {...props}>
            {negative ? <IconRemove /> : <IconAdd />}
        </Fab>
    )
})

export default AddButton
