
import TablePagination from '@material-ui/core/TablePagination'
import React from 'react'

const PaginationControls = ({
    count,
    onPageChange,
    onPerPageChange,
    page,
    perPage,
}) => {
    const handleChangePage = (event, newPage) => onPageChange && onPageChange(newPage)
    const handleChangeRowsPerPage = event => onPerPageChange && onPerPageChange(event.target.value)

    return (
        <TablePagination
            rowsPerPageOptions={[10, 20, 50]}
            component="nav"
            count={count}
            rowsPerPage={perPage}
            page={page - 1} // TablePagination page is zero indexed
            backIconButtonProps={{ 'aria-label': 'previous page' }}
            nextIconButtonProps={{ 'aria-label': 'next page' }}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage} />
    )
}

export default PaginationControls
