import { makeStyles } from '@material-ui/core/styles'
import React, { Fragment } from 'react'
import FilterSort from 'components/FilterSort/FilterSort'
import PaginationControls from 'components/PaginationControls/PaginationControls'
import { useFilters } from 'hooks/useFilters'
const flex = {
    alignItems: 'center',
    display: 'flex',
}

const useStyles = makeStyles({
    container: {
        ...flex,
        justifyContent: ({ hasExtraHeader }) => hasExtraHeader
            ? 'space-between'
            : 'flex-end',
    },
    inner: flex
})

const PaginationWrapper = ({
    baseUrl,
    children,
    options,
    query = {},
    renderHeader,
    count,
    as
}) => {
    const classes = useStyles({ hasExtraHeader: renderHeader })
    const { apply } = useFilters(query, baseUrl, as)
    const page = parseInt(query.page, 10) || 1
    const perPage = parseInt(query.per_page, 10) || 10
    const handlePageChange = p => apply({ page: p + 1 }) // TablePagination page is zero indexed
    const handlePerPageChange = pp => apply({ per_page: pp, page: 1 })
    const handleSelect = selected => apply({ _sort: selected && selected.value })

    const controlProps = {
        count: count,
        page: page,
        perPage: perPage,
        onPageChange: handlePageChange,
        onPerPageChange: handlePerPageChange,
    }

    return (
        <Fragment>
            <div className={classes.container}>
                {renderHeader && renderHeader()}
                <div className={classes.inner}>
                    <PaginationControls {...controlProps} />
                    {options && (
                        <FilterSort
                            options={options}
                            selected={query._sort}
                            onSelect={handleSelect} />
                    )}
                </div>
            </div>
            {children}
            <PaginationControls {...controlProps} />
        </Fragment>
    )
}
PaginationWrapper.defaultProps = {
    options: null
}

export default PaginationWrapper
