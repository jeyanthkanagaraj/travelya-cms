import Typography from '@material-ui/core/Typography'
import React from 'react'
import { useUI } from 'context/UIContext'

const PageTitle = () => {
    const { pageTitle } = useUI()

    return (
        <Typography variant="h6" noWrap>
            {pageTitle}
        </Typography>
    )
}

export default PageTitle
