import { AuthenticationRequired } from 'components/Authentication/Authentication'
import React from 'react'
import Navigation from 'components/Navigation/Navigation'
import Notification from 'components/Notification/Notification'
import Login from 'views/Login/Login'

export const Layout = ({ children }) => {
    return (
        <AuthenticationRequired roles={['admin', 'authenticated']}>
            <Navigation>
                {children}
                <Notification />
            </Navigation>
            <AuthenticationRequired.Fallback>
                <Login />
            </AuthenticationRequired.Fallback>
        </AuthenticationRequired>
    )
}
