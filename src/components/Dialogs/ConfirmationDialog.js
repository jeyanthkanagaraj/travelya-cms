
import Button from '@material-ui/core/Button'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogContentText from '@material-ui/core/DialogContentText'
import DialogTitle from '@material-ui/core/DialogTitle'
import React from 'react'

const ConfirmationDialog = ({ open, options, onCancel, onConfirm }) => {
    const {
        title,
        description,
        confirmationText,
        cancellationText,
    } = options

    return (
        <Dialog fullWidth open={open} onClose={onCancel}>
            {title && <DialogTitle>{title}</DialogTitle>}
            {description && (
                <DialogContent>
                    <DialogContentText>{description}</DialogContentText>
                </DialogContent>
            )}
            <DialogActions>
                <Button onClick={onCancel}>
                    {cancellationText}
                </Button>
                <Button color="primary" onClick={onConfirm}>
                    {confirmationText}
                </Button>
            </DialogActions>
        </Dialog>
    )
}

export default ConfirmationDialog
