import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'
import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'
import { makeStyles } from '@material-ui/core/styles'
import React, { Fragment, useState } from 'react'
import Form from 'components/Form'
import TextField from 'components/Form/TextField'
import { api } from 'lib/api'

const AirportDialog = ({ open, close, editAirport, onSuccess }) => {
    const classes = useStyles()
    const [loading, setLoading] = useState(false)

    const handleSubmit = async formState => {
        const { values, touched } = formState
        setLoading(true)

        // edit existing airport
        if (editAirport) {
            const { id } = editAirport
            const updated = Object.keys(touched)
            if (!updated.length) return

            const data = updated.reduce((payload, key) => {
                payload[key] = values[key]
                return payload
            }, {})

            try {
                const response = await api.airport.patch({ id }, { data })
                onSuccess && onSuccess(response.data)
            }
            catch (error) {
                console.error(error)
            }
            setLoading(false)
            return
        }

        // create new airport
        try {
            await api.airports.post({ data: values })
            onSuccess && onSuccess()
        }
        catch (error) {
            console.error(error)
        }
        setLoading(false)
    }

    return (
        <Dialog open={open} onClose={close}>
            <DialogTitle>{editAirport ? 'Edit ' : 'New '}Airport</DialogTitle>
            <Form onSubmit={handleSubmit} values={editAirport || {}}>
                <DialogContent>
                    <div className={classes.contentContainer}>
                        <Fragment>
                            <TextField required name="name" label="Name" />
                            <TextField required name="code" label="Code" />
                            <TextField required name="address" label="Address" />
                            <TextField required name="city" label="City" />
                            <TextField required name="state" label="State" />
                            <TextField required name="zip" label="Zip" />
                            <TextField required name="country" label="Country" />
                        </Fragment>
                    </div>
                </DialogContent>
                <DialogActions>
                    <div className={classes.actionWrapper}>
                        <Button
                            disabled={loading}
                            variant="contained"
                            onClick={close}
                            className={classes.cancel}>
                            Cancel
                        </Button>
                        <Button
                            disabled={loading}
                            variant="contained"
                            color="primary"
                            type="submit">
                            {loading ? <CircularProgress size={24} /> : 'Submit'}
                        </Button>
                    </div>
                </DialogActions>
            </Form>
        </Dialog>
    )
}

export default AirportDialog

const useStyles = makeStyles(() => {
    return {
        contentContainer: {
            width: 500,
            maxWidth: '95vw'
        },
        cancel: {
            marginRight: '1rem'
        },
        actionWrapper: {
            display: 'flex',
            justifyContent: 'space-between',
            width: '100%',
            padding: '1rem'
        },
    }
})
