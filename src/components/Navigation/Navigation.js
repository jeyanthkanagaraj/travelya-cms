import AppBar from '@material-ui/core/AppBar'
import ButtonBase from '@material-ui/core/ButtonBase'
import Collapse from '@material-ui/core/Collapse'
import Drawer from '@material-ui/core/Drawer'
import List from '@material-ui/core/List'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import { makeStyles } from '@material-ui/core/styles'
import Toolbar from '@material-ui/core/Toolbar'
import IconAirplane from '@material-ui/icons/AirplanemodeActive'
import IconBusiness from '@material-ui/icons/Business'
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft'
import ChevronRightIcon from '@material-ui/icons/ChevronRight'
import IconExitToApp from '@material-ui/icons/ExitToApp'
import IconExpandLess from '@material-ui/icons/ExpandLess'
import IconExpandMore from '@material-ui/icons/ExpandMore'
import IconCompass from '@material-ui/icons/Explore'
import IconHotel from '@material-ui/icons/Hotel'
import IconLocation from '@material-ui/icons/LocationOn'
import IconPeople from '@material-ui/icons/People'
import clsx from 'clsx'
import Link from 'next/link'
import React, { Fragment, useState } from 'react'
import IconLogo from 'components/Icons/IconLogo'
import PageTitle from 'components/PageTitle/PageTitle'

const navigation = {
    DESTINATIONS: {
        name: 'Destinations',
        icon: IconLocation,
        href: '/destinations',
        as: '/destinations'
    },
    COUNTRIES: {
        name: 'Countries',
        icon: IconCompass,
        href: '/countries',
        as: '/countries'
    },
    AIRPORTS: {
        name: 'Airports',
        icon: IconBusiness,
        href: '/airports',
        as: '/airports'
    },
    FLIGHTS: {
        name: 'Flights',
        icon: IconAirplane,
        href: '/flights',
        as: '/flights'
    },
    HOTELS: {
        name: 'Hotels',
        icon: IconHotel,
        href: '/hotels',
        as: '/hotels'
    },
    USERS: {
        name: 'Users',
        icon: IconPeople,
        href: '/users',
        as: '/users',
    },
}

const Navigation = ({ children }) => {
    const classes = useStyles()
    const [open, setOpen] = useState(true)
    const [nested, setNested] = useState({})

    //const auth = useAuth()
    const handleDrawerOpen = () => setOpen(true)
    const handleDrawerClose = () => setOpen(false)

    const handleNestedExpand = (key, nestedOpen) => event => {
        event.stopPropagation()
        setNested({
            ...nested,
            [key]: nestedOpen,
        })
    }

    return (
        <div className={classes.root}>
            <AppBar
                position="fixed"
                className={clsx(classes.appBar, {
                    [classes.appBarShift]: open,
                })}>
                <Toolbar>
                    <PageTitle />
                </Toolbar>
            </AppBar>
            <Drawer
                variant="permanent"
                className={clsx(classes.drawer, {
                    [classes.drawerOpen]: open,
                    [classes.drawerClose]: !open,
                })}
                classes={{
                    paper: clsx({
                        [classes.drawerOpen]: open,
                        [classes.drawerClose]: !open,
                    }),
                }}
                open={open}>
                <div className={clsx({
                    [classes.iconOpen]: open,
                    [classes.iconClosed]: !open
                })}>
                    <Link as="/dashboard" href="/dashboard">
                        <div className={classes.logo}>
                            <IconLogo />
                        </div>
                    </Link>
                </div>
                <List>
                    {Object.entries(navigation).map(([key, item]) => {
                        const Icon = item.icon
                        return (
                            <Fragment key={key}>
                                <Link as={item.as} href={item.href}>
                                    <ListItem button className={classes.listItem}>
                                        <ListItemIcon className={classes.lightText}>
                                            <Icon />
                                        </ListItemIcon>
                                        <ListItemText
                                            primary={item.name}
                                            className={classes.lightText} />
                                        {item.nested
                                            ? nested[key]
                                                ? (
                                                    <IconExpandLess
                                                        className={classes.icons}
                                                        onClick={handleNestedExpand(key, false)} />
                                                )
                                                : (
                                                    <IconExpandMore
                                                        className={classes.icons}
                                                        onClick={handleNestedExpand(key, true)} />
                                                )
                                            : null}
                                    </ListItem>
                                </Link>
                                {item.nested && (
                                    <Collapse in={!!nested[key]} timeout="auto" unmountOnExit>
                                        <List component="div" disablePadding className={classes.nestedList}>
                                            {item.nested
                                                //.filter(nestedItem => permissionCheck(nestedItem.permissions))
                                                .map(nestedItem => {
                                                    const NestedIcon = nestedItem.icon
                                                    return (
                                                        <Link
                                                            as={nestedItem.as}
                                                            href={nestedItem.href}
                                                            key={nestedItem.href}>
                                                            <ListItem button className={classes.listItem}>
                                                                <ListItemIcon className={classes.lightText}>
                                                                    <NestedIcon />
                                                                </ListItemIcon>
                                                                <ListItemText
                                                                    primary={nestedItem.name}
                                                                    className={classes.lightText} />
                                                            </ListItem>
                                                        </Link>
                                                    )
                                                })}
                                        </List>
                                    </Collapse>
                                )}
                            </Fragment>
                        )
                    })}
                </List>
                <List className={classes.logoutContainer}>
                    <ListItem button className={classes.listItem}>
                        <ListItemIcon className={classes.lightText}>
                            <IconExitToApp />
                        </ListItemIcon>
                        <ListItemText primary="Logout" className={classes.lightText} />
                    </ListItem>
                </List>
            </Drawer>
            <div className={clsx({
                [classes.toggleOpen]: open,
                [classes.toggleClosed]: !open
            })}>
                <ButtonBase
                    onClick={open ? handleDrawerClose : handleDrawerOpen}
                    className={classes.toggleButton}>
                    {open
                        ? <ChevronLeftIcon />
                        : <ChevronRightIcon />
                    }
                </ButtonBase>
            </div>
            <main className={classes.content}>
                <div className={classes.toolbar} />
                {children}
            </main>
        </div>
    )
}

const drawerWidth = 225
const appBarHeight = 70

const useStyles = makeStyles(theme => ({
    root: {
        display: 'flex',
    },
    appBar: {
        height: appBarHeight,
        marginLeft: theme.spacing(8) + 6,
        width: `calc(100% - ${theme.spacing(8) + 6}px)`,
        zIndex: theme.zIndex.drawer + 1,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
    },
    appBarShift: {
        marginLeft: drawerWidth,
        width: `calc(100% - ${drawerWidth}px)`,
        transition: theme.transitions.create(['width', 'margin'], {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    menuButton: {
        marginRight: 36,
    },
    hide: {
        display: 'none',
    },
    drawer: {
        width: drawerWidth,
        flexShrink: 0,
        whiteSpace: 'nowrap',
    },
    drawerOpen: {
        backgroundColor: theme.palette.primary.main,
        width: drawerWidth,
        overflow: 'hidden',
        maxWidth: '100%',
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
    },
    drawerClose: {
        backgroundColor: theme.palette.common.dark,
        transition: theme.transitions.create('width', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.leavingScreen,
        }),
        overflowX: 'hidden',
        width: theme.spacing(8) + 6,
    },
    toolbar: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'flex-end',
        padding: theme.spacing(0, 1),
        ...theme.mixins.toolbar,
    },
    content: {
        flexGrow: 1,
        padding: theme.spacing(4),
    },
    toggleClosed: {
        position: 'fixed',
        left: theme.spacing(7),
        transition: theme.transitions.create('left', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
        top: 105,
        zIndex: 1300
    },
    toggleOpen: {
        position: 'fixed',
        left: drawerWidth - 15,
        transition: theme.transitions.create('left', {
            easing: theme.transitions.easing.sharp,
            duration: theme.transitions.duration.enteringScreen,
        }),
        top: 105,
        zIndex: 1300
    },
    toggleButton: {
        backgroundColor: 'white',
        height: 30,
        width: 30,
        borderRadius: 20,
        boxShadow: theme.elevation(1)
    },
    lightText: {
        color: theme.palette.common.white
    },
    icons: {
        cursor: 'pointer',
        maxWidth: '100%',
        maxHeight: appBarHeight,
        '& > *': {
            fill: theme.palette.common.white
        }
    },
    iconClosed: {
        padding: theme.spacing(1.5),
        height: appBarHeight,
        '& > svg': {
            maxHeight: theme.spacing(5) + 5,
            height: theme.spacing(5) + 5,
        }
    },
    iconOpen: {
        padding: theme.spacing(2, 0, 0, 0),
    },
    listItem: {
        paddingLeft: theme.spacing(3),
        '&:hover': {
            background: theme.palette.primary.main,
            color: `${theme.palette.common.dark} !important`,
        },
    },
    logoutContainer: {
        position: 'absolute',
        bottom: 0,
        width: '100%',
    },
    nestedList: {
        boxShadow: `inset 0px 8px 8px -8px ${theme.palette.common.dark}, inset 0px -8px 8px -8px ${theme.palette.common.dark}`,
        background: theme.palette.common.medium,
    },
    logo: {
        padding: theme.spacing(0, 2),
    }
}))

export default Navigation
