import cookie from 'js-cookie'
import Error from 'next/error'
import router from 'next/router'
import React, { useState } from 'react'
import Loading from 'components/Loading/Loading'
import { auth } from 'lib/firebase'
import { http } from 'lib/http'

export const AuthenticationRequired = props => {
    const roles = props.roles
    let authenticated = auth.currentUser

    //if (Array.isArray(roles)) {
    //    authenticated = roles.includes(auth?.role?.type)
    //}
    //
    //
    const [loading, setLoading] = useState(true)

    auth.onAuthStateChanged(async authUser => {
        if (authUser) {
            const token = await authUser.getIdToken()
            cookie.set('userToken', token)
            http.setToken(token)
            if (router.pathname === '/') {
                router.push('/dashboard')
            }
        }
        else {
            cookie.remove('userToken')
        }
        setLoading(false)
    })

    const { children, fallback } = React.Children.toArray(props.children)
        .reduce((acc, child) => {
            child.type.displayName === 'Fallback'
                ? (acc.fallback = child)
                : acc.children.push(child)
            return acc
        }, {
            children: [],
            fallback: null
        })

    if (loading) {
        return <Loading />
    }

    if (authenticated && 'children' in props) {
        return children
    }

    if (!authenticated && fallback) {
        return fallback
    }

    return <Error statusCode={401} />
}

const Fallback = ({ children }) => children || null
Fallback.displayName = 'Fallback'

AuthenticationRequired.Fallback = Fallback
