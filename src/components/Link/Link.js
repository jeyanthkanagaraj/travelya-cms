
import { makeStyles } from '@material-ui/core/styles'
import NextLink from 'next/link'

const useStyles = makeStyles(theme => ({
    anchor: {
        textDecoration: 'none',
        color: theme.palette.common.dark,
        transition: '.2s all',
        padding: theme.spacing(1),
        '&:hover': {
            background: theme.palette.info.main,
            color: theme.palette.common.light,
            transition: '.2s all',
        }
    }
}))

const Link = ({ as, children, href }) => {
    const classes = useStyles()
    return (
        <NextLink as={as} href={href}>
            <a className={classes.anchor}>{children}</a>
        </NextLink>
    )
}

export default Link
