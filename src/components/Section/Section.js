import Paper from '@material-ui/core/Paper'
import { makeStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import React from 'react'

const Section = ({ title, children, button }) => {
    const classes = useStyles({ title })
    return (
        <div className={classes.root}>
            <div className={classes.titleContainer}>
                {title && <Typography variant="subtitle1" className={classes.subtitle}>{title}</Typography>}
                {button}
            </div>
            <Paper className={classes.paper}>
                {children}
            </Paper>
        </div>

    )
}
export default Section

const useStyles = makeStyles(() => {
    return {
        root: { marginBottom: '.5rem' },
        subtitle: { padding: 5 },
        titleContainer: {
            width: '100%',
            display: 'flex',
            justifyContent: 'space-between',
            marginBottom: '.5rem'
        },
        paper: {
            padding: '.2rem'
        }
    }
})
