import Fade from '@material-ui/core/Fade'
import { makeStyles } from '@material-ui/core/styles'
import React from 'react'
import { useForm } from 'components/Form'

const useStyles = makeStyles(theme => {
    return {
        errorText: {
            color: theme.palette.error.main,
            fontWeight: 'bold',
            margin: theme.spacing(1, 0),
            '&:first-letter': {
                textTransform: 'uppercase',
            }
        },
    }
})

const ErrorText = ({ name, ...extra }) => {
    const classes = useStyles()
    const { errors } = useForm()
    const error = errors[name]

    return (
        <Fade in={Boolean(error)}>
            <div {...extra} className={classes.errorText}>
                {error}
            </div>
        </Fade>
    )
}

export default ErrorText
