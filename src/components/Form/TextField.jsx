import { makeStyles } from '@material-ui/core/styles'
import MuiTextField from '@material-ui/core/TextField'
import React, { useEffect } from 'react'
import { useForm } from 'components/Form'
import ErrorText from 'components/Form/ErrorText/ErrorText'

const useStyles = makeStyles(theme => ({
    input: {
        pointerEvents: ({ disabled }) => disabled ? 'none' : 'inherit'
    },
    wrapper: {
        marginBottom: theme.spacing(2),
        width: '100%',
    }
}))

const TextField = ({ dark, disabled, name, validation, ...extra }) => {
    const classes = useStyles({ dark, disabled })
    const {
        onBlur,
        onChange,
        registerField,
        unregisterField,
        values,
    } = useForm()

    useEffect(() => {
        registerField(name, validation)
        return () => unregisterField(name)
    }, [
        name,
        validation,
        registerField,
        unregisterField
    ])

    const handleBlur = () => onBlur(name)
    const handleChange = e => onChange(name, e.target.value)

    return (
        <div className={classes.wrapper}>
            <MuiTextField
                {...extra}
                className={classes.input}
                disabled={disabled}
                fullWidth
                onBlur={handleBlur}
                onChange={handleChange}
                size="small"
                variant="filled"
                value={values[name] || ''} />
            <ErrorText name={name} />
        </div>
    )
}
TextField.defaultProps = {
    dark: false,
}

export default TextField
