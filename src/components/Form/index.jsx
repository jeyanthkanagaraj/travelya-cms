import isEmpty from 'lodash.isempty'
import React, {
    createContext,
    useContext,
    useReducer,
    useRef,
    useEffect,
} from 'react'

const Context = createContext()
Context.displayName = 'FormContext'

export const useForm = () => useContext(Context)

export const actions = {
    setBlur: 'SET_BLUR',
    setErrors: 'SET_ERROR',
    setHasUpdate: 'SET_HAS_UPDATE',
    setValue: 'SET_VALUE',
    setValues: 'SET_VALUES',
    resetForm: 'RESET_FORM',
}

const initialState = {
    errors: {},
    hasUpdate: false,
    values: {},
    touched: {},
}

const reducer = (state, action) => {
    switch (action.type) {
        case actions.setBlur:
            return { ...state, touched: { ...state.touched, ...action.payload } }
        case actions.setErrors:
            return { ...state, errors: action.payload }
        case actions.setHasUpdate:
            return { ...state, hasUpdate: action.payload }
        case actions.setValue:
            return { ...state, values: { ...state.values, ...action.payload } }
        case actions.setValues:
            return { ...state, values: { ...state.values, ...action.payload } }
        case actions.resetForm:
            return {
                ...initialState,
                hasUpdate: false,
                reset: true,
                values: {
                    ...initialState.values,
                    ...action.payload
                }
            }
        default:
            return state
    }
}

const regex = /_/gi

const validationMap = {
    required: (field, value) => ({ [field]: { valid: !!value, message: `${field.replace(regex, ' ')} is required` } }),
    number: (field, value) => ({ [field]: { valid: new RegExp(/[0-9]/).test(value), message: `${field.replace(regex, ' ')} is not a number` } }),
    email: (field, value) => ({
        [field]: {
            valid: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value),
            message: 'Please enter a valid email address'
        }
    })
}

const Form = React.forwardRef(({
    children,
    Element = 'form',
    onChange,
    onReset,
    onSubmit,
    onUpdate,
    values,
    ...extra
}, ref) => {
    const fieldRegistry = useRef({})
    const [state, dispatch] = useReducer(reducer, {
        ...initialState,
        values: values || {}
    })

    useEffect(() => {
        dispatch({ type: actions.setValues, payload: values })
    }, [values])

    const handleBlur = key => {
        dispatch({ type: actions.setBlur, payload: { [key]: true } })
    }

    const handleChange = (key, value) => {
        dispatch({ type: actions.setValue, payload: { [key]: value } })
        onChange && onChange({ ...state, values: { ...state.values, [key]: value } })

        if (!state.hasUpdate) {
            onUpdate && onUpdate({ ...state, hasUpdate: true })
            dispatch({ type: actions.setHasUpdate, payload: true })
        }
    }

    const handleSubmit = async e => {
        e.preventDefault()
        if (!isEmpty(state.errors)) dispatch({ type: actions.setErrors, payload: {} })

        try {
            await handleValidate()
            onSubmit && onSubmit(state, dispatch)
            dispatch({ type: actions.setHasUpdate, payload: false })
        }
        catch (errors) {
            dispatch({ type: actions.setErrors, payload: errors })
        }
    }

    const handleReset = e => {
        e.preventDefault()
        onReset && onReset({ ...state, hasUpdate: false, values })
        dispatch({ type: actions.resetForm, payload: values })
    }

    const handleValidate = async () => new Promise((resolve, reject) => {
        const errors = Object.keys(fieldRegistry.current).reduce((acc, key) => {
            const current = fieldRegistry.current[key]
            const validation = current.validation.map(validationFn => {
                let func
                switch (typeof validationFn) {
                    case 'function': func = validationFn; break
                    case 'string': func = validationMap[validationFn]; break
                    default: break
                }
                const value = func(key, state.values[key], state.values)
                if (!value[key].valid) return value[key].message
            }).filter(f => f)
            const error = validation[0]
            if (error) acc[key] = error
            return acc
        }, {})

        return Object.keys(errors).length > 0 ? reject(errors) : resolve()
    })

    const registerField = (fieldName, validation) => {
        fieldRegistry.current[fieldName] = {
            validation: validation || []
        }
    }

    const unregisterField = fieldName => {
        delete fieldRegistry.current[fieldName]
    }

    return (
        <Context.Provider
            value={{
                ...state,
                onBlur: handleBlur,
                onChange: handleChange,
                registerField,
                unregisterField,
            }}>
            <Element {...extra} onReset={handleReset} onSubmit={handleSubmit} ref={ref}>
                {children}
            </Element>
        </Context.Provider>
    )
})

export default Form
