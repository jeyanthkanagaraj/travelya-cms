import getConfig from 'next/config'
import Router from 'next/router'
import NProgress from 'nprogress'
import React, { useEffect, useRef } from 'react'
import { defaultTheme } from 'styles/theme'

NProgress.configure({
    showSpinner: false,
    minimum: 0.1,
    easing: 'ease',
    speed: 400,
    trickleSpeed: 100,
})

const showAfter = 200

const NProgressContainer = () => {
    const timer = useRef(null)

    useEffect(() => {
        const handleRouteChangeEnd = () => {
            clearTimeout(timer.current)
            NProgress.done()
        }

        const handleRouteChangeStart = () => {
            clearTimeout(timer.current)
            NProgress.start()
            timer.current = setTimeout(NProgress.start, showAfter)
        }

        Router.events.on('routeChangeComplete', handleRouteChangeEnd)
        Router.events.on('routeChangeError', handleRouteChangeEnd)
        Router.events.on('routeChangeStart', handleRouteChangeStart)

        return () => {
            clearTimeout(timer.current)
            Router.events.off('routeChangeComplete', handleRouteChangeEnd)
            Router.events.off('routeChangeError', handleRouteChangeEnd)
            Router.events.off('routeChangeStart', handleRouteChangeStart)
        }
    }, [])

    return (
        <style jsx global>{`
            #nprogress {
                pointer-events: none;
            }
            #nprogress .bar {
                background: ${defaultTheme.palette.secondary.main};
                position: absolute;
                z-index: 2500;
                top: 0;
                left: 0;
                width: 100%;
                height: 4px;
            }
            #nprogress .peg {
                display: block;
                position: absolute;
                right: 0px;
                width: 100px;
                height: 100%;
                opacity: 1;
                -webkit-transform: rotate(3deg) translate(0px, -4px);
                -ms-transform: rotate(3deg) translate(0px, -4px);
                transform: rotate(3deg) translate(0px, -4px);
            }
            .nprogress-custom-parent {
                overflow: hidden;
                position: relative;
            }
            .nprogress-custom-parent #nprogress .bar {
                position: absolute;
            }
        `}</style>
    )
}

export default NProgressContainer
