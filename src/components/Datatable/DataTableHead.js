import Checkbox from '@material-ui/core/Checkbox'
import TableCell from '@material-ui/core/TableCell'
import TableHead from '@material-ui/core/TableHead'
import TableRow from '@material-ui/core/TableRow'
import PropTypes from 'prop-types'
import React from 'react'

const DataTableHead = ({
    columns,
    hideSelectAll = false,
    onSelectAllClick,
    numSelected,
    rowCount,
}) => {
    return (
        <TableHead>
            <TableRow>
                {columns.map(columnDef => {
                    const field = columnDef.field
                    if (field === 'select') {
                        if (hideSelectAll) {
                            return (
                                <TableCell align="left" key={field} padding="default">
                                    Enabled
                                </TableCell>
                            )
                        }

                        return (
                            <TableCell key={field} padding="checkbox">
                                <Checkbox
                                    indeterminate={numSelected > 0 && numSelected < rowCount}
                                    checked={numSelected === rowCount}
                                    inputProps={{ 'aria-labelledby': 'datatable-checkbox-header' }}
                                    onChange={onSelectAllClick} />
                            </TableCell>
                        )
                    }

                    return (
                        <TableCell
                            key={field}
                            align={columnDef.options?.align ?? 'left'}
                            padding="default"
                        >
                            {columnDef.header}
                        </TableCell>
                    )
                })}
            </TableRow>
        </TableHead>
    )
}

DataTableHead.propTypes = {
    columns: PropTypes.array.isRequired,
    numSelected: PropTypes.number.isRequired,
    onSelectAllClick: PropTypes.func.isRequired,
    rowCount: PropTypes.number.isRequired,
}

export default DataTableHead
