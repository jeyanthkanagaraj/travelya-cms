import IconButton from '@material-ui/core/IconButton'
import Menu from '@material-ui/core/Menu'
import MenuItem from '@material-ui/core/MenuItem'
import { lighten, makeStyles } from '@material-ui/core/styles'
import Toolbar from '@material-ui/core/Toolbar'
import Tooltip from '@material-ui/core/Tooltip'
import Typography from '@material-ui/core/Typography'
import IconMoreVert from '@material-ui/icons/MoreVert'
import clsx from 'clsx'
import PropTypes from 'prop-types'
import React, { Fragment, useState } from 'react'

const useToolbarStyles = makeStyles(theme => ({
    root: {
        paddingLeft: theme.spacing(2),
        paddingRight: theme.spacing(1),
    },
    highlight: theme.palette.type === 'light'
        ? {
            color: theme.palette.secondary.main,
            backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
        : {
            color: theme.palette.text.primary,
            backgroundColor: theme.palette.secondary.dark,
        },
    title: {
        flex: '1 1 100%',
    },
    menu: {
        maxHeight: theme.spacing(27),
    }
}))

const DataTableToolbar = ({
    heading,
    headingActions,
    numSelected,
    onActionClick,
}) => {
    const [anchorEl, setAnchorEl] = useState(null)
    const classes = useToolbarStyles()
    const open = Boolean(anchorEl)

    const handleClick = event => setAnchorEl(event.currentTarget)
    const handleClose = () => setAnchorEl(null)
    const handleActionClick = action => {
        onActionClick && onActionClick(action)
        handleClose()
    }

    return (
        <Toolbar
            className={clsx(classes.root, {
                [classes.highlight]: numSelected > 0,
            })}
        >
            {numSelected > 0 ? (
                <Typography className={classes.title} color="inherit" variant="subtitle1">
                    {numSelected} selected
                </Typography>
            ) : (
                <Typography className={classes.title} variant="h6" id="tableTitle">
                    {heading}
                </Typography>
            )}
            {numSelected > 0 ? (
                <Fragment>
                    <Tooltip title="Actions">
                        <IconButton
                            aria-label="actions"
                            aria-controls="long-menu"
                            aria-haspopup="true"
                            onClick={handleClick}
                        >
                            <IconMoreVert />
                        </IconButton>
                    </Tooltip>
                    <Menu
                        id="long-menu"
                        anchorEl={anchorEl}
                        keepMounted
                        open={open}
                        onClose={handleClose}
                        PaperProps={{
                            className: classes.menu,
                        }}
                    >
                        {Object.values(headingActions).map(action => (
                            <MenuItem key={action.value} onClick={() => handleActionClick(action)}>
                                {action.name}
                            </MenuItem>
                        ))}
                    </Menu>
                </Fragment>
            ) : null}
        </Toolbar>
    )
}

DataTableToolbar.propTypes = {
    numSelected: PropTypes.number.isRequired,
}

export default DataTableToolbar
