import React from 'react'
import Link from 'components/Link/Link'

const LinkCell = ({ baseUrl, children, row, id }) => {
    return (
        <Link as={`/${baseUrl}/${id || row.id}`} href={`/${baseUrl}/[id]`}>
            {children}
        </Link>
    )
}

export default LinkCell
