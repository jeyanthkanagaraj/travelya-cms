import Checkbox from '@material-ui/core/Checkbox'
import Paper from '@material-ui/core/Paper'
import { makeStyles } from '@material-ui/core/styles'
import Table from '@material-ui/core/Table'
import TableBody from '@material-ui/core/TableBody'
import TableCell from '@material-ui/core/TableCell'
import TableRow from '@material-ui/core/TableRow'
import { useRouter } from 'next/router'
import React, { useEffect } from 'react'
import DataTableHead from './DataTableHead'
import DataTableToolbar from './DataTableToolbar'

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing(3),
    },
    paper: {
        width: '100%',
        marginBottom: theme.spacing(2),
    },
    table: {
        minWidth: 750,
    },
    tableWrapper: {
        overflowX: 'auto',
    },
    visuallyHidden: {
        border: 0,
        clip: 'rect(0 0 0 0)',
        height: 1,
        margin: -1,
        overflow: 'hidden',
        padding: 0,
        position: 'absolute',
        top: 20,
        width: 1,
    },
}))

const Datatable = ({
    columns,
    data = [],
    heading,
    headingActions,
    onActionClick,
    onSelect,
}) => {
    const classes = useStyles()
    const [selected, setSelected] = React.useState([])
    const router = useRouter()

    const handleSelectAllClick = event => {
        const selectedData = event.target.checked ? data : []
        setSelected(selectedData)
        onSelect && onSelect(selectedData)
    }

    useEffect(() => {
        setSelected([])
    }, [router.asPath])

    const handleClick = (event, name) => {
        const selectedIndex = selected.indexOf(name)
        let newSelected = []

        if (selectedIndex === -1) {
            newSelected = newSelected.concat(selected, name)
        }
        else if (selectedIndex === 0) {
            newSelected = newSelected.concat(selected.slice(1))
        }
        else if (selectedIndex === selected.length - 1) {
            newSelected = newSelected.concat(selected.slice(0, -1))
        }
        else if (selectedIndex > 0) {
            newSelected = newSelected.concat(
                selected.slice(0, selectedIndex),
                selected.slice(selectedIndex + 1),
            )
        }

        setSelected(newSelected)
        onSelect && onSelect(newSelected)
    }

    const isSelected = id => selected.findIndex(row => row.id === id) !== -1

    return (
        <div className={classes.root}>
            <Paper className={classes.paper}>
                <DataTableToolbar
                    heading={heading}
                    headingActions={headingActions}
                    numSelected={selected.length}
                    onActionClick={onActionClick} />
                <div className={classes.tableWrapper}>
                    <Table
                        className={classes.table}
                        aria-labelledby="tableTitle"
                        size="small"
                        aria-label="data table">
                        <DataTableHead
                            columns={columns}
                            numSelected={selected.length}
                            onSelectAllClick={handleSelectAllClick}
                            rowCount={data.length} />
                        <TableBody>
                            {data.map((row, index) => {
                                const isItemSelected = isSelected(row.id)
                                const labelId = `datatable-checkbox-${index}`
                                return (
                                    <TableRow
                                        hover
                                        role="checkbox"
                                        aria-checked={isItemSelected}
                                        tabIndex={-1}
                                        key={`${index}-${row.id}`}
                                        selected={isItemSelected}>
                                        {columns.map(columnDef => {
                                            const field = columnDef.field
                                            if (field === 'select') {
                                                return (
                                                    <TableCell
                                                        key={field}
                                                        padding="checkbox"
                                                        onClick={event => handleClick(event, row)}>
                                                        <Checkbox
                                                            checked={isItemSelected}
                                                            inputProps={{ 'aria-labelledby': labelId }} />
                                                    </TableCell>
                                                )
                                            }

                                            let cell

                                            if (typeof row[field] !== 'object') {
                                                cell = row[field]
                                            }

                                            if (columnDef.cell && typeof columnDef.cell === 'function') {
                                                cell = columnDef.cell(row, index)
                                            }

                                            return (
                                                <TableCell
                                                    align={columnDef.options?.align ?? 'left'}
                                                    id={labelId}
                                                    key={field}
                                                    padding={columnDef.padding || 'default'}
                                                    scope="row">
                                                    {cell}
                                                </TableCell>
                                            )
                                        })}
                                    </TableRow>
                                )
                            })}
                        </TableBody>
                    </Table>
                </div>
            </Paper>
        </div>
    )
}

export default Datatable
