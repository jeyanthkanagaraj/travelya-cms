
import getConfig from 'next/config'
import React, {
    createContext,
    useContext,
    useState
} from 'react'

const ENV = getConfig().publicRuntimeConfig

const Context = createContext()
Context.displayName = 'UIContext'

export const useUI = () => useContext(Context)

export const UIContextProvider = ({ children }) => {
    const [pageTitle, setPageTitle] = useState(ENV.BRAND_NAME)

    return (
        <Context.Provider
            value={{
                pageTitle,
                setPageTitle,
            }}
        >
            {children}
        </Context.Provider>
    )
}
