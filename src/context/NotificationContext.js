import React, {
    createContext,
    useContext,
    useState,
} from 'react'

const Context = createContext()
Context.displayName = 'NotificationContext'

export const useNotification = () => useContext(Context)

export const NotificationContextProvider = ({ children }) => {
    const [notification, setNotification] = useState({
        message: '',
        category: '',
        open: false
    })
    const dispatch = (message, category) => setNotification({ message, category, open: true })
    const close = () => setNotification({ message: '', category: '', open: false })

    return (
        <Context.Provider value={{
            ...notification,
            close,
            dispatch,
        }}
        >
            {children}
        </Context.Provider>
    )
}
