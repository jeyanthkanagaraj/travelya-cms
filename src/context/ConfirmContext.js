
import React, { Fragment, useCallback, useState } from 'react'
import ConfirmationDialog from 'components/Dialogs/ConfirmationDialog'

export const ConfirmContext = React.createContext()

const defaultOptions = {
    title: 'Please Confirm',
    description: '',
    confirmationText: 'Ok',
    cancellationText: 'Cancel',
}

export const ConfirmContextProvider = ({ children }) => {
    const [options, setOptions] = useState(defaultOptions)
    const [resolveReject, setResolveReject] = useState([])
    const [resolve, reject] = resolveReject

    const confirm = useCallback((opts = {}) => {
        return new Promise((res, rej) => {
            setOptions({ ...defaultOptions, ...opts })
            setResolveReject([res, rej])
        })
    }, [])

    const handleClose = useCallback(() => { setResolveReject([]) }, [])

    const handleCancel = useCallback(() => {
        reject()
        handleClose()
    }, [reject, handleClose])

    const handleConfirm = useCallback(() => {
        resolve()
        handleClose()
    }, [resolve, handleClose])

    return (
        <Fragment>
            <ConfirmContext.Provider value={confirm}>
                {children}
            </ConfirmContext.Provider>
            <ConfirmationDialog
                open={resolveReject.length === 2}
                options={options}
                onCancel={handleCancel}
                onConfirm={handleConfirm} />
        </Fragment>
    )
}
